import React, { useState } from "react";
import { FaDollarSign } from "react-icons/fa";
import { MdPeopleAlt, MdOutlineShowChart } from "react-icons/md";
import { RiBankCardLine } from "react-icons/ri";

import Overview from "../Overview/Overview";
import Analytics from "../Analytics/Analytics";

const Home = () => {
  // console.log(BarChartData)

  const [dashboardTab, setDashboardTab] = useState(1);

  return (
    <div className="min-h-screen overflow-hidden relative sm:h-max bg-black text-white p-4">
      <div className="header-top-container px-4">
        <div className="header-top-menu ">
          <nav className="">
            <ul className="flex items-start  px-4 text-sm text-gray-500 ">
              <li className="px-2">
                <a href="">Mail</a>
              </li>
              <li className="px-2">
                <a href="">Dashboard</a>
              </li>
              <li className="px-2">
                <a href="">Cards</a>
              </li>
              <li className="px-2">
                <a href="">Tasks</a>
              </li>
              <li className="px-2">
                <a href="">Playground</a>
              </li>
              <li className="px-2">
                <a href="">Forms</a>
              </li>
              <li className="px-2">
                <a href="">Music</a>
              </li>
              <li className="px-2">
                <a href="">Authentication</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>

      {/* Header Main Menu */}
      <div className="header-main-menu relative pt-6 overflow-hidden">
        <div
          className="grid grid-cols-3 py-4 px-2
            text-gray-500 border rounded-t-lg border-gray-800"
        >
          <div className="col-span-2">
            <div className="flex items-center flex-wrap text-wrap">
              <div className="flex items-center gap-x-1 rounded-md border p-1 border-gray-800">
                <img
                  src="https://placehold.co/20x20"
                  alt=""
                  className="rounded-full object-contain h-4 w-4"
                />
                <button className="text-sm text-white px-2">Alice Koch</button>
              </div>
              {/* Tab */}
              <div className="p-1">
                <nav className=" px-2 ">
                  <ul className="flex gap-x-4 text-sm font-bold">
                    <li>
                      <a
                        href=""
                        onClick={() => setDashboardTab(1)}
                        className={
                          (dashboardTab === 1
                            ? "text-red-500" + "block" + "bg-green-600"
                            : "text-gray-600") + " cursor-pointer"
                        }
                      >
                        Overview
                      </a>
                    </li>
                    <li
                      onClick={() => setDashboardTab(1)}
                      className={
                        (dashboardTab === 2
                          ? "text-red-500" + "block" + "bg-green-600"
                          : "text-gray-600") + " cursor-pointer"
                      }
                    >
                      <a href="">Customer</a>
                    </li>
                    <li>
                      <a href="">Products</a>
                    </li>
                    <li>
                      <a href="">Settings</a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>

          <div className="flex">
            <form action="" className="flex gap-x-2">
              <input
                type="search"
                name=""
                id=""
                className="w-full bg-gray-900 px-4 border border-gray-500
                rounded-md outline-none"
                placeholder="Search...."
              />
              <img
                src="https://placehold.co/30x30"
                alt=""
                className="max-w-15 max-h-15 rounded-full"
              />
            </form>
          </div>
        </div>
      </div>

      {/* Dashboard Content */}

      <div className="rounded-b-md border  border-gray-800">
        <section className="dashboard-content relative px-4">
          <div className="flex justify-between items-center">
            <div>
              <h1 className="text-3xl py-4">Dashboard</h1>
              <div className="dashboard-tab flex gap-x-4 py-1 bg-gray-500 px-2 rounded-md">
                <p
                  onClick={() => setDashboardTab(1)}
                  className={
                    (dashboardTab === 1 ? "bg-black"+" px-2" +" rounded-md" : "text-gray-400") +
                    " cursor-pointer" +
                    " hover:bg-black" 
                  }
                >
                  Overview
                </p>
                <p
                  onClick={() => setDashboardTab(2)}
                  className={
                    (dashboardTab === 2 ? "bg-black"+" px-2" +" rounded-md" : "text-gray-400") +
                    " cursor-pointer"
                  }
                >
                  Analytics
                </p>
                <p
                  onClick={() => setDashboardTab(2)}
                  className={
                    (dashboardTab === 3 ? "bg-black"+" px-2" +" rounded-md" : "text-gray-400") +
                    " cursor-pointer"
                  }
                >
                  Reports
                </p>
                <p
                  onClick={() => setDashboardTab(2)}
                  className={
                    (dashboardTab === 4 ? "bg-black"+" px-2" +" rounded-md" : "text-gray-400") +
                    " cursor-pointer"
                  }
                >
                  {" "}
                  Notification
                </p>
              </div>
            </div>
            <div className="flex justify-evenly gap-x-2">
              <p className="border text-sm py-1 border-gray-500 px-5 rounded-md">
                Jan,202023 - Feb 09,2023
              </p>
              <button className="bg-white text-black px-4 rounded-md">
                Download
              </button>
            </div>
          </div>
        </section>

        {/* Dashboard Card */}
        <section className="px-4">
          <div className="py-4 flex justify-evenly items-center gap-4">
            <div className="p-4 border  border-gray-800 rounded-md w-full">
              <p className="flex justify-between text-sm">
                Total Revenue{" "}
                <span>
                  <FaDollarSign />
                </span>
              </p>
              <h2 className="text-2xl">$45,250.22</h2>
              <small className="text-gray-500">+20% from last month</small>
            </div>

            <div className="p-4 border  border-gray-800 rounded-md w-full">
              <p className="flex justify-between text-sm">
                Subscriptions{" "}
                <span>
                  <MdPeopleAlt />
                </span>
              </p>
              <h2 className="text-2xl">$45,250.22</h2>
              <small className="text-gray-500">+20% from last month</small>
            </div>

            <div className="p-4 border  border-gray-800 rounded-md w-full">
              <p className="flex justify-between text-sm">
                Sales{" "}
                <span>
                  <RiBankCardLine />
                </span>
              </p>
              <h2 className="text-2xl">$45,250.22</h2>
              <small className="text-gray-500">+20% from last month</small>
            </div>

            <div className="p-4 border  border-gray-800 rounded-md w-full">
              <p className="flex justify-between text-sm">
                Active Now{" "}
                <span>
                  <MdOutlineShowChart />
                </span>
              </p>
              <h2 className="text-2xl">$45,250.22</h2>
              <small className="text-gray-500">+20% from last month</small>
            </div>
          </div>
        </section>

        {/* Dashboard Chart */}
        <section className="px-4">
          <div className={dashboardTab === 1 ? "block" : "hidden"}>
            <Overview />
          </div>

          <div className={dashboardTab === 2 ? "block" : "hidden"}>
            <Analytics />
          </div>
        </section>
      </div>
    </div>
  );
};

export default Home;
