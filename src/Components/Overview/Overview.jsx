import React from 'react'
import { Chart as ChartJs } from "chart.js/auto";
import { Bar } from "react-chartjs-2";
import BarChartData from "../BarchartData.json";

const Overview = () => {
  return (
    <div>
          <div className=" flex  justify-center gap-2  py-4">
            {/* Chart */}
            <div className="w-2/3 border  border-gray-800 rounded-md ">
              <Bar
                data={{
                  labels: BarChartData.map((data) => data.month),
                  datasets: [
                    {
                      label: "Overview",
                      data: BarChartData.map((data) => data.value),
                      backgroundColor: ["white"],
                    },
                  ],
                }}
              />
            </div>

            <div className="w-1/3 px-4 border  border-gray-800 rounded-md py-2">
              {/* sales Header */}
              <div>
                <h4 className="text-lg">Recent Sales</h4>
                <p className="text-gray-500 text-sm">
                  You made 255 sales this month
                </p>
              </div>

              {/* Sales item */}
              <div className="flex flex-wrap justify-between py-4 ">
                <div className="flex flex-wrap justify-center items-center gap-2">
                  <div>
                    <img
                      src="https://placehold.co/20x20"
                      alt=""
                      className="rounded-full"
                    />
                  </div>
                  <div>
                    <h5 className="text-sm">John Doe</h5>
                    <p className="text-sm text-gray-500">john@gmail.com</p>
                  </div>
                </div>

                <div>
                  <p>+$999.99</p>
                </div>
              </div>
              {/* Sales item */}
              <div className="flex flex-wrap justify-between py-4 ">
                <div className="flex flex-wrap justify-center items-center gap-2">
                  <div>
                    <img
                      src="https://placehold.co/20x20"
                      alt=""
                      className="rounded-full"
                    />
                  </div>
                  <div>
                    <h5 className="text-sm">John Doe</h5>
                    <p className="text-sm text-gray-500">john@gmail.com</p>
                  </div>
                </div>

                <div>
                  <p>+$999.99</p>
                </div>
              </div>

              {/* Sales item */}
              <div className="flex flex-wrap justify-between py-4 ">
                <div className="flex flex-wrap justify-center items-center gap-2">
                  <div>
                    <img
                      src="https://placehold.co/20x20"
                      alt=""
                      className="rounded-full"
                    />
                  </div>
                  <div>
                    <h5 className="text-sm">John Doe</h5>
                    <p className="text-sm text-gray-500">john@gmail.com</p>
                  </div>
                </div>

                <div>
                  <p>+$999.99</p>
                </div>
              </div>

              <div className="flex flex-wrap justify-between py-4 ">
                <div className="flex flex-wrap justify-center items-center gap-2">
                  <div>
                    <img
                      src="https://placehold.co/20x20"
                      alt=""
                      className="rounded-full"
                    />
                  </div>
                  <div>
                    <h5 className="text-sm">John Doe</h5>
                    <p className="text-sm text-gray-500">john@gmail.com</p>
                  </div>
                </div>

                <div>
                  <p>+$999.99</p>
                </div>
              </div>

              <div className="flex flex-wrap justify-between py-4 ">
                <div className="flex flex-wrap justify-center items-center gap-2">
                  <div>
                    <img
                      src="https://placehold.co/20x20"
                      alt=""
                      className="rounded-full"
                    />
                  </div>
                  <div>
                    <h5 className="text-sm">John Doe</h5>
                    <p className="text-sm text-gray-500">john@gmail.com</p>
                  </div>
                </div>

                <div>
                  <p>+$999.99</p>
                </div>
              </div>
            </div>
          </div>
    </div>
  )
}

export default Overview